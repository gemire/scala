var captial = Map("US" -> "Washington", "France" -> "Paris")
captial += ("Japan" -> "Tokyo")
println(captial("France"))

def factorial(x: BigInt):BigInt =
  if(x == 0) 1 else x * factorial(x - 1)

factorial(30)

import java.math.BigInteger
def factorialByJava(x: BigInteger): BigInteger =
  if(x == BigInteger.ZERO)
    BigInteger.ZERO
  else
    x.multiply(factorialByJava(x.subtract(BigInteger.ONE)))

val name = "Gemire"
val nameHasUpperCase = name.exists(_.isUpper)

val msg = "Hello, world!"

def max(x: Int, y: Int): Int = {
  if(x > y) x
  else y
}

def greet() = println("hello,world")

val newmsg =Array("Concice", "is", "nice")
newmsg.foreach(arg => println(arg))
newmsg.foreach((arg: String) => println(arg))
newmsg.foreach(println)