package com.gemire.demo

/**
 * Created by gemire on 2015/5/27.
 */
object ScalaApp {
  def main(args: Array[String]) {
    val user: User = User("hengzhang.zju@gmail.com")
    val nullable: Option[User] = Some(user)
    nullable.map((user: User) => {
      print("Found user:%s".format(user))
    })
    print(
      """
        |hello,world
        |it is gemire's demo!
      """.stripMargin
    )
  }
}
