package com.gemire.demo

/**
 * Created by gemire on 2015/5/27.
 */
class Hello {
    def sayHello(name: String) = s"Hello, $name!"
}
