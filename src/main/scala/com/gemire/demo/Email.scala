package com.gemire.demo

/**
 * A classs, which represents a valid email address
 * @param adress A _valid_ adress
 * Created by gemire on 2015/5/27.
 */
case class Email(val adress: String) {
  val matcher = """([\w\.]+@[\w\.]+)""".r.pattern.matcher(adress)
  if (!matcher.matches()) {
    throw new IllegalArgumentException("Address is invalid!")
  }

}

object Email {
  implicit def stringToEmail(string: String): Email = Email(string)
}
