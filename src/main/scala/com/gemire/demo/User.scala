package com.gemire.demo

/**
 * Created by gemire on 2015/5/27.
 */
case class User(val contact: Email) {
  override def toString(): String = s"[Email: ${contact.adress}]"

}
