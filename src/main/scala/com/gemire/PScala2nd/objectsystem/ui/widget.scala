// src/main/scala/progscala2/objectsystem/ui/Widget.scala
package com.gemire.PScala2nd.objectsystem.ui

abstract class Widget {
  def draw(): Unit
  override def toString() = "(widget)"
}
