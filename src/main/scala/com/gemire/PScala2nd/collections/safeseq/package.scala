// src/main/scala/progscala2/collections/safeseq/package.scala
package com.gemire.PScala2nd.collections

package object safeseq {
  type Seq[T] = collection.immutable.Seq[T]
}
