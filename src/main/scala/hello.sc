import com.gemire.demo.Hello

val hello = new Hello
println(hello.sayHello("Scala"))

val x = 3
x+2

val r = """([_A-Za-z0-9-]+(?:\.[_A-Za-z0-9-\+]+)*)(@[A-Za-z0-9-]+(?:\.[A-Za-z0-9-]+)*(?:\.[A-Za-z]{2,})) ?""".r
r.replaceAllIn("abc.edf+jianli@gmail.com   hello@gmail.com.cn", (m => "*****" + m.group(2)))

for (i <- 1 to 4) {
  print("hello" + i)
}


def hello(m: String) = {
  var a = 1
  for(i <- m){
    a *= i
  }

//  return a

  if( a % 2 == 0) a = a-1
  else a
}

val m = "Scala"
hello(m)