package com.gemire.demo

import org.scalatest.FunSuite

/**
 * Created by gemire on 2015/5/27.
 */
class EmailTest extends FunSuite {
  test("Email with valid adress") {
    val email = Email("hengzhang.zju@gmail.com")
    assert(email.adress != null)
  }

  test("Email with invalid adress") {
    intercept[IllegalArgumentException] {
      Email("gemire")
    }
  }
}
