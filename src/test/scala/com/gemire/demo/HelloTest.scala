package com.gemire.demo

import org.scalatest.FunSuite

/**
 * Created by gemire on 2015/5/27.
 */
class HelloTest extends FunSuite {

  test("SayHello method works correctly") {
    val hello = new Hello
    assert(hello.sayHello("Scala") == "Hello, Scala!")
  }

}
